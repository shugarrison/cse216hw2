import java.util.Arrays;
import java.util.List;

public class Quadrilateral implements Positionable, TwoDShape {

    private final TwoDPoint[] vertices = new TwoDPoint[4];

    public Quadrilateral(double... vertices) { 
        this(TwoDPoint.ofDoubles(vertices));
    }

    public Quadrilateral(List<TwoDPoint> vertices) {
        int n = 0;
        for (TwoDPoint p : vertices) this.vertices[n++] = p;
        if (!isMember(vertices))
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getCanonicalName()));
    }
    
    public TwoDPoint[] getVertices() {
    	return vertices;
    }
    
    /**
     * Given a list of four points, adds them as the four vertices of this quadrilateral in the order provided in the
     * list. This is expected to be a counterclockwise order of the four corners.
     *
     * @param points the specified list of points.
     * @throws IllegalStateException if the number of vertices provided as input is not equal to four.
     */
    @Override
    public void setPosition(List<? extends Point> points) {
    	for (int i = 0; i < 4; i++) {
    		vertices[i] = (TwoDPoint)points.get(i);
    	}
    }

    @Override
    public List<TwoDPoint> getPosition() {
        return Arrays.asList(vertices);
    }

    /**
     * @return the lengths of the four sides of the quadrilateral. Since the setter {@link Quadrilateral#setPosition(List)}
     *         expected the corners to be provided in a counterclockwise order, the side lengths are expected to be in
     *         that same order.
     */
    protected double[] getSideLengths() {
    	double length1 = Math.sqrt(Math.pow(vertices[0].getCoorx() - vertices[1].getCoorx(),2) + Math.pow(vertices[0].getCoory() - vertices[1].getCoory(),2));
    	double length2 = Math.sqrt(Math.pow(vertices[1].getCoorx() - vertices[2].getCoorx(),2) + Math.pow(vertices[1].getCoory() - vertices[2].getCoory(),2));
    	double length3 = Math.sqrt(Math.pow(vertices[2].getCoorx() - vertices[3].getCoorx(),2) + Math.pow(vertices[2].getCoory() - vertices[3].getCoory(),2));
    	double length4 = Math.sqrt(Math.pow(vertices[3].getCoorx() - vertices[0].getCoorx(),2) + Math.pow(vertices[3].getCoory() - vertices[0].getCoory(),2));
    	double[] doubleArray = new double[] {length1, length2, length3, length4};
        return doubleArray; // TODO
    }

    @Override
    public int numSides() { return 4; }

    @Override
    public boolean isMember(List<? extends Point> vertices) { return vertices.size() == 4; }
}
