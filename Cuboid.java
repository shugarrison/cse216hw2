import java.util.List;
import java.util.ArrayList;

// TODO : a missing interface method must be implemented in this class to make it compile. This must be in terms of volume().
public class Cuboid implements ThreeDShape, SurfaceArea {

    private final ThreeDPoint[] vertices = new ThreeDPoint[8];

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the vertices are provided in
     * the order as shown in the figure given in the homework document (from v0 to v7).
     * 
     * @param vertices the specified list of vertices in three-dimensional space.
     */
    public Cuboid(List<ThreeDPoint> vertices) {
        if (vertices.size() != 8)
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));
        int n = 0;
        for (ThreeDPoint p : vertices) this.vertices[n++] = p;
    }

    @Override
    public double volume() {
    	double length1 = Math.sqrt(Math.pow(vertices[0].coordinates()[0]-vertices[1].coordinates()[0],2) + Math.pow(vertices[0].coordinates()[1]-vertices[1].coordinates()[1],2) + Math.pow(vertices[0].coordinates()[2]-vertices[1].coordinates()[2],2));
    	double length2 = Math.sqrt(Math.pow(vertices[1].coordinates()[0]-vertices[2].coordinates()[0],2) + Math.pow(vertices[1].coordinates()[1]-vertices[2].coordinates()[1],2) + Math.pow(vertices[1].coordinates()[2]-vertices[2].coordinates()[2],2));
    	double length3 = Math.sqrt(Math.pow(vertices[2].coordinates()[0]-vertices[7].coordinates()[0],2) + Math.pow(vertices[2].coordinates()[1]-vertices[7].coordinates()[1],2) + Math.pow(vertices[2].coordinates()[2]-vertices[7].coordinates()[2],2));
        return length1 * length2 * length3; // TODO
    }

    @Override
    public ThreeDPoint center() {
    	double coorx = 0;
    	double coory = 0;
    	double coorz = 0;
    	
    	for (int i = 0; i < 8; i++) {
    		coorx = coorx + vertices[i].coordinates()[0];
    		coory = coory + vertices[i].coordinates()[1];
    		coorz = coorz + vertices[i].coordinates()[2];
    	}
    	return new ThreeDPoint(coorx/8, coory/8, coorz/8);
    	// TODO
    }
    
    public static Cuboid random() {
    	double lengthx = Math.random()*1000;
    	double lengthy = Math.random()*1000;
    	double lengthz = Math.random()*1000;
    	
    	double x = Math.random()*1000 - 500;
    	double y = Math.random()*1000 - 500;
    	double z = Math.random()*1000 - 500;
    	
    	List<ThreeDPoint> lst = new ArrayList<ThreeDPoint>();
    	lst.add(new ThreeDPoint(x, y ,z));
    	lst.add(new ThreeDPoint(x - lengthx, y, z));
    	lst.add(new ThreeDPoint(x - lengthx, y - lengthy, z));
    	lst.add(new ThreeDPoint(x, y - lengthy, z));
    	lst.add(new ThreeDPoint(x, y - lengthy, z + lengthz));
    	lst.add(new ThreeDPoint(x, y, z + lengthz));
    	lst.add(new ThreeDPoint(x - lengthx, y, z + lengthz));
    	lst.add(new ThreeDPoint(x - lengthx, y - lengthy, z + lengthz));
    	
    	return new Cuboid(lst);
    	
    }
    
    public double surfaceArea() {
    	double length1 = Math.sqrt(Math.pow(vertices[0].coordinates()[0]-vertices[1].coordinates()[0],2) + Math.pow(vertices[0].coordinates()[1]-vertices[1].coordinates()[1],2) + Math.pow(vertices[0].coordinates()[2]-vertices[1].coordinates()[2],2));
    	double length2 = Math.sqrt(Math.pow(vertices[1].coordinates()[0]-vertices[2].coordinates()[0],2) + Math.pow(vertices[1].coordinates()[1]-vertices[2].coordinates()[1],2) + Math.pow(vertices[1].coordinates()[2]-vertices[2].coordinates()[2],2));
    	double length3 = Math.sqrt(Math.pow(vertices[2].coordinates()[0]-vertices[7].coordinates()[0],2) + Math.pow(vertices[2].coordinates()[1]-vertices[7].coordinates()[1],2) + Math.pow(vertices[2].coordinates()[2]-vertices[7].coordinates()[2],2));
    	double area1 = length1 * length2 * 2;
    	double area2 = length1 * length3 * 2;
    	double area3 = length2 * length3 * 2;
    	return area1 + area2 + area3;
    }
    
    public int compareTo(ThreeDShape c) {
    	if (this.volume() < c.volume()) {
    		return -1;
    	}
    	else if (this.volume() > c.volume()) {
    		return 1;
    	}
    	else {
    		return 0;
    	}
    }

}
