import java.util.*;

public class Ordering {

    static class XLocationComparator implements Comparator<TwoDShape> {
        @Override public int compare(TwoDShape o1, TwoDShape o2) {
        	double x1,x2;
        	if (o1.numSides() == 0) {
        		Circle shape1 = (Circle)o1;
        		x1 = shape1.center().coordinates()[0] - shape1.getRadius();
        	}
        	else {
        		Quadrilateral shape1 = (Quadrilateral)o1;
        		double x = 0;
        		for (int i = 0; i < 4; i++) {
        			if (x > shape1.getPosition().get(i).getCoorx()) {
        				x = shape1.getPosition().get(i).getCoorx();
        			}
        		}
        		x1 = x;
        	}
        	if (o2.numSides() == 0) {
        		Circle shape2 = (Circle)o2;
        		x2 = shape2.center().coordinates()[0] - shape2.getRadius();
        	}
        	else {
        		Quadrilateral shape2 = (Quadrilateral)o2;
        		double x = 0;
        		for (int i = 0; i < 4; i++) {
        			if (x > shape2.getPosition().get(i).getCoorx()) {
        				x = shape2.getPosition().get(i).getCoorx();
        			}
        		}
        		x2 = x;
        	}
        	return (int)(x1 - x2);
        }
    }

    static class AreaComparator implements Comparator<SymmetricTwoDShape> {
        @Override public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2) {
            return (int)(o1.area() - o2.area()); // TODO
        }
    }

    static class SurfaceAreaComparator implements Comparator<ThreeDShape> {
        @Override public int compare(ThreeDShape o1, ThreeDShape o2) {
        	double s1, s2;
        	if (o1.getClass() == Sphere.class) {
        		Sphere shape1 = (Sphere)o1;
        		s1 = shape1.surfaceArea();
        	}
        	else {
        		Cuboid shape1 = (Cuboid)o1;
        		s1 = shape1.surfaceArea();
        	}
        	if (o2.getClass() == Sphere.class) {
        		Sphere shape2 = (Sphere)o2;
        		s2 = shape2.surfaceArea();
        	}
        	else {
        		Cuboid shape2 = (Cuboid)o2;
        		s2 = shape2.surfaceArea();
        	}
        	return (int)(s1 - s2);
        }
    }

    // TODO: there's a lot wrong with this method. correct it so that it can work properly with generics.
    static <T> void copy(Collection<? extends T> source, Collection<T> destination) {
        destination.addAll(source);
    }

    public static void main(String[] args) {
        List<TwoDShape>          shapes          = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        List<ThreeDShape>        threedshapes    = new ArrayList<>();

        /*
         * uncomment the following block and fill in the "..." constructors to create actual instances. If your
         * implementations are correct, then the code should compile and yield the expected results of the various
         * shapes being ordered by their smallest x-coordinate, area, volume, surface area, etc. */

        List<TwoDPoint> rectangle = new ArrayList<TwoDPoint>();
        rectangle.add(new TwoDPoint(0,0));
        rectangle.add(new TwoDPoint(0,1));
        rectangle.add(new TwoDPoint(1,1));
        rectangle.add(new TwoDPoint(1,0));
        
        List<TwoDPoint> square = new ArrayList<TwoDPoint>();
        square.add(new TwoDPoint(1,1));
        square.add(new TwoDPoint(1,3));
        square.add(new TwoDPoint(3,3));
        square.add(new TwoDPoint(3,1));
        
        symmetricshapes.add(new Rectangle(rectangle));
        symmetricshapes.add(new Square(square));
        symmetricshapes.add(new Circle(1,1,2));

        copy(symmetricshapes, shapes); // note-1 //
        System.out.print(shapes.toString());
        

        // sorting 2d shapes according to various criteria
        shapes.sort(new XLocationComparator());
        symmetricshapes.sort(new XLocationComparator());
        System.out.print(symmetricshapes.toString());
        symmetricshapes.sort(new AreaComparator());
        System.out.print(symmetricshapes.toString());

        // sorting 3d shapes according to various criteria
        Collections.sort(threedshapes);
        threedshapes.sort(new SurfaceAreaComparator());

        /*
         * if your changes to copy() are correct, uncommenting the following block will also work as expected note that
         * copy() should work for the line commented with 'note-1' while at the same time also working with the lines
         * commented with 'note-2' and 'note-3'. */

        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();
        
        doubles.add(1.0);
        doubles.add(2.0);
        
        squares.add(new Square(square));

        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //
        System.out.println(numbers.toString());
        System.out.println(quads.toString());
        
        Cuboid c = Cuboid.random();
        System.out.println(c);
    }
}
