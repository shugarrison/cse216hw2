/**
 * An unmodifiable point in the three-dimensional space. The coordinates are specified by exactly three doubles (its
 * <code>x</code>, <code>y</code>, and <code>z</code> values).
 */
public class ThreeDPoint implements Point {
	private double coorx, coory, coorz;

    public ThreeDPoint(double x, double y, double z) {
        coorx = x;
        coory = y;
        coorz = z;
    }
    
    public double getCoorx() {
    	return coorx;
    }
    
    public void setCoorx(double x) {
    	coorx = x;
    }
    
    public double getCoory() {
    	return coory;
    }
    
    public void setCoory(double y) {
    	coory = y;
    }
    
    public double getCoorz() {
    	return coorz;
    }
    
    public void setCoorz(double z) {
    	coorz = z;
    }
    /**
     * @return the (x,y,z) coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
    	double[] doubleArray = new double[] {coorx, coory, coorz};
        return doubleArray; // TODO
    }
}
