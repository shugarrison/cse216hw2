
public class Sphere implements ThreeDShape, SurfaceArea {
	
	private ThreeDPoint center;
	private double radius;
	
	public void setRadius(double r) { 
		this.radius = r; 
	}

    public double getRadius() { 
    	return radius; 
    }
	
    public static Sphere random() {
    	return new Sphere(Math.random()*1000 - 500, Math.random()*1000 - 500, Math.random()*1000 - 500, Math.random()*1000 - 500);
    }
    
	public Sphere(double centerx, double centery, double centerz, double radius) {
		this.center = new ThreeDPoint(centerx, centery, centerz);
		this.radius = radius;
	}
	
	public Point center() {
		return center;
	}
	
	public double volume() {
		return (4/3) * Math.PI * Math.pow(radius, 3);
	}
	
	public double surfaceArea() {
		return 4 * Math.PI * Math.pow(radius, 2);
	}
	
	public int compareTo(ThreeDShape c) {
    	if (this.volume() < c.volume()) {
    		return -1;
    	}
    	else if (this.volume() > c.volume()) {
    		return 1;
    	}
    	else {
    		return 0;
    	}
    }
}
