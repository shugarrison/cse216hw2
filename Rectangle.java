import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape {
	
	public Rectangle(List<TwoDPoint> vertices) {
		super(vertices);
	}

    /**
     * The center of a rectangle is calculated to be the point of intersection of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center() {
    	double coorx = 0;
    	double coory = 0;
    	
    	for (int i = 0; i < 4; i++) {
    		coorx = coorx + this.getVertices()[i].getCoorx();
    		coory = coory + this.getVertices()[i].getCoory();
    	}
    	
        return new TwoDPoint(coorx/4, coory/4); // TODO
    }

    @Override
    public boolean isMember(List<? extends Point> vertices) {
        double midx1 = (vertices.get(0).coordinates()[0] + vertices.get(2).coordinates()[0])/2;
        double midy1 = (vertices.get(0).coordinates()[1] + vertices.get(2).coordinates()[1])/2;
        double midx2 = (vertices.get(1).coordinates()[0] + vertices.get(3).coordinates()[0])/2;
        double midy2 = (vertices.get(1).coordinates()[1] + vertices.get(3).coordinates()[1])/2;
        
        return midx1 == midx2 & midy1 == midy2 & vertices.size() == 4;
    }

    @Override
    public double area() {
        double[] sideLengths = this.getSideLengths();
        return sideLengths[0] * sideLengths[1];
    }
}
