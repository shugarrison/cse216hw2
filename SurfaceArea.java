
public interface SurfaceArea {
	
	double surfaceArea();
	
}
