import java.util.List;
import java.util.ArrayList;

/**
 * An unmodifiable point in the standard two-dimensional Euclidean space. The coordinates of such a point is given by
 * exactly two doubles specifying its <code>x</code> and <code>y</code> values.
 */
public class TwoDPoint implements Point {
	private double coorx;
	private double coory;

    public TwoDPoint(double x, double y) {
    	coorx = x;
    	coory = y;
    }
    
    public double getCoorx() {
    	return coorx;
    }
    
    public void setCoorx(double x) {
    	coorx = x;
    }
    
    public double getCoory() {
    	return coory;
    }
    
    public void setCoory(double y) {
    	coory = y;
    }

    /**
     * @return the coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
    	double[] doubleArray = new double[]{coorx, coory};
        return doubleArray;
    }

    /**
     * Returns a list of <code>TwoDPoint</code>s based on the specified array of doubles. A valid argument must always
     * be an even number of doubles so that every pair can be used to form a single <code>TwoDPoint</code> to be added
     * to the returned list of points.
     *
     * @param coordinates the specified array of doubles.
     * @return a list of two-dimensional point objects.
     * @throws IllegalArgumentException if the input array has an odd number of doubles.
     */
    public static List<TwoDPoint> ofDoubles(double... coordinates) throws IllegalArgumentException {
    	List<TwoDPoint> listOfPoints = new ArrayList<TwoDPoint>();
    	if (coordinates.length % 2 == 1) {
    		throw new IllegalArgumentException();
    	}
    	else {
    		for (int i = 0; i < coordinates.length; i = i + 2) {
    			listOfPoints.add(new TwoDPoint(coordinates[i], coordinates[i+1]));
    		}
    	}
        return listOfPoints; // TODO
    }
}
