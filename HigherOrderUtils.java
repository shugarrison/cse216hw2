import java.util.List;
import java.util.ArrayList;
import java.util.function.BiFunction;
import java.util.function.Function;

public class HigherOrderUtils {
	
	public static class FunctionComposition<T,U,R> {
		BiFunction<Function<T,U>,Function<U,R>,Function<T,R>> composition = new BiFunction<Function<T,U>,Function<U,R>,Function<T,R>>() {
			public Function<T,R> apply(Function<T,U> fun1, Function<U,R> fun2) {
				return fun1.andThen(fun2);
			}
		};
		
	}
	
	public interface NamedBiFunction<T,U,R> extends java.util.function.BiFunction<T,U,R> {
		
		String name();
		
	}
	
	public static NamedBiFunction<Double, Double, Double> add = new NamedBiFunction<Double, Double, Double>() {
		public String name() {
			return "add";
		}
		
		public Double apply(Double x, Double y) {
			return x + y;
		}
	};
	
	public static NamedBiFunction<Double, Double, Double> subtract = new NamedBiFunction<Double, Double, Double>() {
		public String name() {
			return "diff";
		}
		
		public Double apply(Double x, Double y) {
			return x - y;
		}
	};
	
	public static NamedBiFunction<Double, Double, Double> multiply = new NamedBiFunction<Double, Double, Double>() {
		public String name() {
			return "mult";
		}
		
		public Double apply(Double x, Double y) {
			return x * y;
		}
	};
	
	public static NamedBiFunction<Double, Double, Double> divide = new NamedBiFunction<Double, Double, Double>() {
		public String name() {
			return "div";
		}
		
		public Double apply(Double x, Double y) {
			if (y == 0) {
				throw new ArithmeticException();
			}
			else {
				return x / y;
			}
		}
	};
	
	public static <T> T zip(List<T> args, List<NamedBiFunction<T, T, T>> bifunctions) {
		for (int i = 0; i < bifunctions.size(); i++) {
			args.set(i+1, bifunctions.get(i).apply(args.get(i), args.get(i+1)));
		}
		return args.get(args.size() - 1);
	}
	
	public static void main(String[] args) {
		List<Double> lst = new ArrayList<Double>();
		lst.add(2.0);
		lst.add(1.0);
		lst.add(3.0);
		lst.add(0.0);
		lst.add(2.0);
		
		List<NamedBiFunction<Double,Double,Double>> bfs = new ArrayList<NamedBiFunction<Double,Double,Double>>();
		bfs.add(add);
		bfs.add(multiply);
		bfs.add(add);
		bfs.add(divide);
		
		System.out.print(HigherOrderUtils.zip(lst, bfs));
		
	}
	
	
}
