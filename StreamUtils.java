import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Comparator;
import java.util.stream.Stream;
import java.util.HashMap;

public class StreamUtils {
	
	public static Collection<String> capitalized(Collection<String> strings) {
		Collection<String> collection = strings.stream().filter(s->Character.isUpperCase(s.charAt(0))).collect(Collectors.toList());
		return collection;
	}
	
	public static String longest(Collection<String> strings, boolean from_start) {
		if (from_start) {
			return strings.stream().max(Comparator.comparingInt(String::length)).get();
		}
		else {
			List<String> string = new ArrayList<String>(strings);
			Collections.reverse(string);
			return string.stream().max(Comparator.comparingInt(String::length)).get();
		}
	}
	
	public static <T extends Comparable<T>> T least(Collection<T> items, boolean from_start) {
		if (from_start) {
			return items.stream().min(Comparator.naturalOrder()).get();
		}
		else {
			List<T> item = new ArrayList<T>(items);
			Collections.reverse(item);
			return item.stream().min(Comparator.naturalOrder()).get();
		}
	}
	
	public static <K, V> List<String> flatten(Map<K, V> aMap) {
		return aMap.entrySet().stream().flatMap(str -> Stream.of(str.getKey().toString() + " -> " + str.getValue().toString())).collect(Collectors.toList());
	}

	public static void main(String[] args) {
		HashMap<Integer, String> hash_map = new HashMap<Integer, String>(); 
		  
        // Mapping string values to int keys 
        hash_map.put(10, "Geeks"); 
        hash_map.put(15, "4"); 
        hash_map.put(20, "Geeks"); 
        hash_map.put(25, "Welcomes"); 
        hash_map.put(30, "You"); 
        
        System.out.print(flatten(hash_map));
        

	}

}
